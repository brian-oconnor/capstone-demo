<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dog Eat Dog World!</title>
</head>
<body>
<form:form commandName="formDog">
   <table>
    <tr>
        <td><form:label path="name">Name:</form:label></td>
        <td><form:input path="name" /></td>
        <td><form:label path="age">Age:</form:label></td>
        <td><form:input path="age"/></td>
        <td><form:label path="breed">Breed:</form:label></td>
        <td><form:input path="breed" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form:form>
</body>
</html>